import React from "react";
import styled, { css } from "styled-components";
import { Button, Dialog } from "evergreen-ui";
import Quagga from "quagga";
import moment from "moment";

const Screen = styled.div`
  display: grid;
  grid-template-rows: 80px 1fr 80px;
  
  box-sizing: border-box;
  height: 100%;
  overflow: hidden;
  overscroll-behavior: none;
  padding: 10px;
  width: 100vw;
`;

const Header = styled.h1`
  display: flex;
  justify-content: space-around;
`;

const MainContent = styled.div`
  display: flex;
  flex-direction: column;
  overflow: hidden;
`;

const Footer = styled.div`
  display: flex;
  justify-content: space-between;
  align-self: end;
  
  height: 42px;
  width: 100%;
`;

const Entry = styled.div`
  display: flex;
  justify-content: space-around;
`;

const Entries = styled.div`
  display: flex;
  flex-direction: column-reverse;
  padding-top: 50px;
`;

const Viewfinder = styled.div<Visibility>`
  ${(_: Visibility) => !_.visible && css`
    display: none;
  `}
  min-height: 100px;
  overflow: hidden;
`;

const ActionButton = styled(Button)`
  height: 100%;
  font-size: medium;
`;

const ClearButton = styled(ActionButton)`
  margin-right: 5px;
`;

interface Visibility {
  visible: boolean;
}

type Database = Record<string, number>

interface State {
  capturing: boolean;
  capturedItems: Database;
  confirmClear?: boolean;
}

const STORAGE_KEY = "TELLERPOINT_BARCODE";

class App extends React.Component<{}, State> {
  readonly state: State = {
    capturing: false,
    capturedItems: (JSON.parse(window.localStorage.getItem(STORAGE_KEY) || "{}") as Database)
  };

  componentWillUnmount() {
    this.stopCapturing();
  }

  private startCapturing = async () => {
    Quagga.init({
      inputStream: {
        name: "Live",
        type: "LiveStream",
        constraints: {
          width: 100
        }
      },
      decoder: {
        readers: ["code_39_reader"],
        debug: {
          drawBoundingBox: true,
          drawScanline: true,
          showPattern: true
        }
      },
      locator: {
        debug: {
          showCanvas: true
        }
      }
    }, (ex: Error) => {
      if (ex) {
        alert("Unable to load app");
        return;
      }

      Quagga.onDetected(async capture => await this.storeItem(capture.codeResult.code));
      Quagga.start();
    });
  };

  private stopCapturing = Quagga.stop;

  private toggleCapturing = async (e: React.SyntheticEvent<HTMLButtonElement>) => {
    e.currentTarget.blur();
    if (this.state.capturing) {
      await this.stopCapturing();
    } else {
      this.startCapturing();
    }
    this.setState({ capturing: !this.state.capturing });
  };

  private storeItem = async (barcode: string) => {
    await document.querySelector("audio")!.play();
    const newItem = {
      barcode,
      timestamp: new Date().getTime()
    };
    const capturedItems = { ...this.state.capturedItems, [newItem.barcode]: newItem.timestamp };
    window.localStorage.setItem(STORAGE_KEY, JSON.stringify(capturedItems));
    this.setState({ capturedItems });
  };

  private confirmClear = () => this.setState({ confirmClear: true });

  private discardConfirmation = () => this.setState({ confirmClear: false });

  private clear = () => {
    window.localStorage.clear();
    this.setState({ capturedItems: {} });
    this.discardConfirmation();
  };

  private downloadCsv = (e: React.SyntheticEvent<HTMLButtonElement>) => {
    e.currentTarget.blur();

    const csv = Object.entries(this.state.capturedItems)
      .map(([barcode, timestamp]) =>
        `${barcode},${moment(timestamp).format("MM/DD/YYYY h:mm a")}`)
      .join("\n");

    const downloadLink = document.createElement("a");
    downloadLink.setAttribute("href", "data:text/csv;charset=utf-8," + encodeURIComponent(csv));
    downloadLink.setAttribute("download", "tellerpoint-barcodes.csv");

    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  };

  render = () => (
    <Screen>
      <Header>Tellerpoint Barcode</Header>

      <MainContent>
        <Viewfinder visible={this.state.capturing} id="interactive" className="viewport" />
        <Entries>
          {Object.entries(this.state.capturedItems).map(([barcode, timestamp]) =>
            <Entry key={barcode}>{barcode} {moment(timestamp).fromNow()}</Entry>)}
        </Entries>
      </MainContent>

      <Footer>
        <div>
          {Object.keys(this.state.capturedItems).length > 0 && (
            <>
              <ClearButton
                intent="danger"
                iconBefore="delete"
                onClick={this.confirmClear}>Clear DB</ClearButton>
              <ActionButton
                intent="success"
                iconBefore="download"
                onClick={this.downloadCsv}>Export</ActionButton>
            </>
          )}
        </div>
        <ActionButton
          appearance="primary"
          iconBefore={this.state.capturing ? "stop" : "camera"}
          onClick={this.toggleCapturing}>{this.state.capturing ? "Stop" : "Start"} capturing</ActionButton>
      </Footer>
      {this.renderStorageClearConfirmation()}
    </Screen>
  );

  private renderStorageClearConfirmation = () => (
    <Dialog
      confirmLabel="Clear"
      cancelLabel="Preserve"
      hasCancel
      hasClose={false}
      intent="danger"
      isShown={this.state.confirmClear}
      onConfirm={this.clear}
      onCancel={this.discardConfirmation}
      title="Clear database"
      topOffset="calc(100vh * 0.5 - 200px)">
      Are you sure you want to delete all captured data?
    </Dialog>
  );
}

export default App;
